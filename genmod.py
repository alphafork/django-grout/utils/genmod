#!/usr/bin/env python
import csv
import sys
import os


def main():
    try:
        csv_file = sys.argv[1]
    except IndexError:
        raise SystemExit("Usage: genmod <input_file.csv>")

    if not os.path.exists(csv_file):
        raise SystemExit(f"File not found: {csv_file}")

    with open(csv_file) as file:
        reader = csv.DictReader(file)
        models = {}
        model_file_name = ""
        model_name = ""
        field_types = {
            # Commonly used fields
            "b": "BooleanField",
            "c": "CharField",
            "d": "DateField",
            "dt": "DateTimeField",
            "e": "EmailField",
            "f": "FileField",
            "fp": "FilePathField",
            "i": "IntegerField",
            "im": "ImageField",
            "s": "SlugField",
            "t": "TextField",
            # Relationship fields
            "fk": "ForeignKey",
            "mm": "ManyToManyField",
            "oo": "OneToOneField",
            # Other fields
            "af": "AutoField",
            "ba": "BigAutoField",
            "de": "DecimalField",
            "du": "DurationField",
            "fl": "FloatField",
            "ip": "GenericIPAddressField",
            "j": "JSONField",
            "p": "PositiveIntegerField",
            "pb": "PositiveBigIntegerField",
            "ps": "PositiveSmallIntegerField",
            "sa": "SmallAutoField",
            "si": "SmallIntegerField",
            "ti": "TimeField",
            "u": "URLField",
            "uu": "UUIDField",
        }

        for row in reader:
            verbose_name = ""
            field_options = ""
            model_file_name = row["model_file_name"] or model_file_name
            model_parents_name = row["model_parents_name"] or "models.Model"
            model_name = row["model_name"] or model_name

            if model_file_name not in models:
                models[model_file_name] = {}
            if model_name not in models.get(model_file_name, {}):
                models[model_file_name][model_name] = {
                    "fields": "",
                    "str": "",
                    "parents": "(" + model_parents_name + ")",
                }

            if row["field_name"] and row["field_type"]:
                field = (
                    "    "
                    + row["field_name"]
                    + " = models."
                    + field_types[row["field_type"].strip()]
                )
            else:
                continue
            if row["related_field"]:
                field_options += row["related_field"]
                if row["field_type"] != "mm":
                    field_options += ", on_delete=models.CASCADE"
                verbose_name += ", verbose_name="
            if row["verbose_name"]:
                verbose_name += "'" + row["verbose_name"].strip() + "'"
                field_options += verbose_name
            if row["field_type"] == "c":
                field_options += ", max_length=" + (row["max_length"] or "255")
            if row["null"] == "1":
                field_options += ", null=True"
            if row["blank"] == "1":
                field_options += ", blank=True"
            if row["unique"] == "1":
                field_options += ", unique=True"
            if row["default"]:
                field_options += ", default=" + row["default"].strip()
            if row["use_as_str"] == "1":
                models[model_file_name][model_name]["str"] = row["field_name"]
            if row["db_comment"]:
                field_options += ", db_comment=" + "'" + row["db_comment"] + "'"
            if row["custom_options"]:
                field_options += row["custom_options"]
            field_options = "(" + field_options.strip(", ") + ")\n"
            if model_name not in models.get(model_file_name, {}):
                models[model_file_name][model_name]["fields"] = field + field_options
            else:
                models[model_file_name][model_name]["fields"] += field + field_options

        for model_file in models:
            output_dir = sys.argv[2] if len(sys.argv) >= 3 else "models"
            if output_dir == "models":
                os.makedirs("models", exist_ok=True)
            output_file = open(output_dir + "/" + model_file + ".py", "w")
            output = "from django.db import models\n"
            for model in models[model_file]:
                output += (
                    "\n\nclass "
                    + model
                    + models[model_file][model]["parents"]
                    + ":\n"
                    + models[model_file][model]["fields"]
                )
                if models[model_file][model]["str"]:
                    output += (
                        "\n    def __str__(self):\n        return self."
                        + models[model_file][model]["str"]
                    )
            output_file.write(output)


if __name__ == "__main__":
    main()
