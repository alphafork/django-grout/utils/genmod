# genmod

Generate django models from CSV file.


## Motivation

Unlike other projects out there that help to import data from CSV files, the
idea of this project is to generate actual model files.


## Usage

`python genmod.py <input_file.csv> [output_dir]`

If `output_dir` is not given, writes to `models` dir in the working directory.


### Input CSV file format

```
'model_file_name','model_name','field_name','field_type','verbose_name',
'max_length','null','blank', 'unique','default','use_as_str','related_field',
'db_comment','custom_options'
```


## License
[GPL-3.0-or-later](LICENSE)


## Contact

Alpha Fork Technologies

Email: [connect@alphafork.com](mailto:connect@alphafork.com)

